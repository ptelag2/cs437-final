import time
import RPi.GPIO as GPIO 

GPIO.setmode(GPIO.BCM) 
GPIO.setup(4, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)

GPIO.output(4, True)
time.sleep(1)
GPIO.output(4, False)

GPIO.output(3, True)
time.sleep(1)
GPIO.output(3, False)
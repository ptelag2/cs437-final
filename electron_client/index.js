const { send } = require('process');
var fs = require('fs');
document.onkeydown = updateKey;
document.onkeyup = resetKey;

var server_port = 65431;
var server_addr = "192.168.1.169";   // the IP address of your Raspberry PI

function client(){
    
    const net = require('net');
    var input = document.getElementById("message").value;

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');

        // send the message
        client.write(`${input}\r\n`);
    });

    // get the data from the server
    client.on('data', (data) => {
        document.getElementById("motion_state").innerHTML = data;
        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });
}
function receiveMsg() {
    const net = require('net');

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        writeLog('connected to server!');
        client.write(`status\r\n`);

    });
    
    // get the data from the server
    client.on('data', (data) => {
        const state_vals = data.toString().split(",", 3);
        document.getElementById("motion_state").innerHTML = state_vals[0];
        document.getElementById("power").innerHTML = state_vals[1];
        // document.getElementById("cpu_usage").innerHTML = state_vals[2];

        console.log(data.toString());
        writeLog("Received Data:" + data.toString())
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
        writeLog('disconnected from server')

    });
}

function sendMsg(msg) {
    const net = require('net');

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${msg}\r\n`);
    });
    client.on('end', () => {
        console.log('disconnected from server');
    });
}
// for detecting which key is been pressed w,a,s,d
function updateKey(e) {

    e = e || window.event;

    if (e.keyCode == '87') {
        // up (w)
        document.getElementById("upArrow").style.color = "green";
        send_data("87");
    }
    else if (e.keyCode == '83') {
        // down (s)
        document.getElementById("downArrow").style.color = "green";
        send_data("83");
    }
    else if (e.keyCode == '65') {
        // left (a)
        document.getElementById("leftArrow").style.color = "green";
        send_data("65");
    }
    else if (e.keyCode == '68') {
        // right (d)
        document.getElementById("rightArrow").style.color = "green";
        send_data("68");
    }
}

// reset the key to the start state 
function resetKey(e) {

    e = e || window.event;

    document.getElementById("upArrow").style.color = "grey";
    document.getElementById("downArrow").style.color = "grey";
    document.getElementById("leftArrow").style.color = "grey";
    document.getElementById("rightArrow").style.color = "grey";
}


// update data for every 50ms
function update_data(dir){
    if (dir == 0) {
        var msg = "ARMED COMMAND";
        console.log(msg);
        writeLog(msg);
        sendMsg('arm');
    }
    else if (dir == 1) {
        var msg = "DISARMED COMMAND";
        console.log(msg);
        writeLog(msg);
        sendMsg('disarm');
    }
    
}
function writeLog(data){
    var currentdate = new Date(); 
    var datetime = "Timestamp: " + (currentdate.getMonth()+1)  + "/" 
                    + currentdate.getDate() + "/"
                    + currentdate.getFullYear() + " @ "  
                    + currentdate.getHours() + ":"  
                    + currentdate.getMinutes() + ":" 
                    + currentdate.getSeconds();

    fs.appendFile('security_log.txt', datetime + ', Msg: ' + data + '\n', function (err) {
        if (err) throw err;
        console.log('started log!');
        });
}
function update_text() {
    setInterval(function(){
        // get image from python server
        receiveMsg();
    }, 200);
}

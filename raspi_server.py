import socket

HOST = "192.168.1.169" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)
power = 40
size = 1024
motion_state = "stop"
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()

    try:
        while 1:
            client, clientInfo = s.accept()
            # print("server recv from: ", clientInfo)
            data = client.recv(size).decode('UTF-8').strip()      # receive 1024 Bytes of message in binary format
            if data:
                # print(data)
                # if data == "forward":
                #     # print("moving forward")
                #     motion_state = "forward"
                #     # picar.forward(power)
                # elif data == "backward":
                #     motion_state = "backward"
                #     # picar.backward(power)
                # elif data == "left":
                #     motion_state = "left"
                #     # picar.turn_left(power)
                # elif data == "right":
                #     motion_state = "right"
                #     # picar.turn_right(power)
                # elif data == "status":
                #     # power = picar.power_read()
                #     # cpu_usage = picar.cpu_usage()
                #     # state_msg = f"{motion_state},{power} Volts, {cpu_usage}%"
                #     # print("state:", state_msg)
                #     state_msg = "Test message from the raspi"
                #     client.sendall(bytes(state_msg, 'utf-8')) # Echo back to client
                # elif data == "stop":
                #     motion_state = "stop"
                #     # picar.stop()   

                if data:
                    if data == 'arm':
                        print("System will be armed eventually")
                    elif data == 'disarm':
                        print("System will be disarmed eventually")
                    elif data == 'status':
                        state_msg = "Armed, Disarmed, Camera"
                        client.sendall(bytes(state_msg, 'utf-8')) # Echo back to client

    except KeyboardInterrupt: 
        print("Closing socket")
        client.close()
        s.close()    
import os
import sys 
import RPi.GPIO as GPIO
import time
import serial
import smtplib
from dotenv import load_dotenv

load_dotenv()

# sys.path.insert(0, '/home/pi/tflite1')

from tflite_1 import cam 
import cv2 

accepted_cards = ['2A CF 7D 19', '2A 60 A9 15']

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.setup(4, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)

armed = False 
armed_pin = 3
disarmed_pin = 4

notification_delay = 1
need_to_notify = True

security_breached = False 

print("Setup Complete")

num = 0

def allow_exit_delay(delay=1, exit_time=15):
    for i in range(exit_time):
        GPIO.output(armed_pin, False) 
        time.sleep(delay)
        GPIO.output(armed_pin, True)
        time.sleep(delay) 
    if not armed:
        GPIO.output(armed_pin, False) 



def toggle_system():
    global armed 
    armed = not armed

    
    GPIO.output(armed_pin, armed)
    GPIO.output(disarmed_pin, not armed)

    if armed:
        allow_exit_delay(exit_time=2)


def alert_resident(message= "Security Breached in your Home"):
    final_message = 'Subject: {}\n\n{}'.format("Home Security Alert: {}".format(time.time()), message)

    USER = os.getenv('USERNAME')
    PASSWORD = os.environ.get('PASSWORD')

    server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    server.login(USER, PASSWORD)
    server.sendmail(USER, "praval.telagi@gmail.com", final_message)
    server.quit()


def flash_alert(num_flashes=5, delay=0.2):

    for i in range(num_flashes):
        GPIO.output(armed_pin, False) 
        time.sleep(delay)
        GPIO.output(armed_pin, True)
        time.sleep(delay) 
    
    if not armed:
        GPIO.output(armed_pin, False) 


if __name__ == '__main__':
    ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
    ser.flush()

    cur_delay_counter = 0 

    GPIO.output(armed_pin, False) 
    GPIO.output(disarmed_pin, True) 
    
    while True:
        if ser.in_waiting > 0:
            line = ser.readline().decode('utf-8').rstrip()
            # print(line)
            
            if line[10:] in accepted_cards:
                print("Access Granted")
                

                toggle_system() 

                if armed:
                    print("System Armed:", armed)
                else:
                    print("System Disarmed")

                    # Reset System State
                    cur_delay_counter = 0
                    need_to_notify = True 
                    security_breached = False 

                
            else:
                print("Access Denied: Informing Residents")

                alert_resident("Unauthorized Card Tried to Arm/Disarm your System")
                # if armed:
                flash_alert()



        input_state = GPIO.input(18)             

        if armed:
            msg = "Issue Detected:"
            if input_state == False:
                print("Door Open")
                security_breached = True
                msg += " Door Opened"
            
            objects_found_from_camera = cam.read_camera()
            print("Objects found:", objects_found_from_camera)
            if ('person' in objects_found_from_camera):
                print("Person Detected")
                security_breached = True 
                msg += " Movement Found"

            
            if security_breached:
                print("Issue Detected")
                cur_delay_counter += 1
                time.sleep(1)

                if cur_delay_counter > notification_delay and need_to_notify:
                    print("Alerting Residents")
                    # print("MESSAGE:", msg)
                    alert_resident(message=msg)
                    need_to_notify = False 


        


        # input_state = GPIO.input(18) 
        # if input_state == False and armed:
        #     print("Door Opened and System Armed")
        #     time.sleep(1)
        #     print("Current Delay Counter: ", cur_delay_counter)
        #     cur_delay_counter += 1 

        #     if cur_delay_counter > notification_delay:
        #         # Alert Resident
        #         alert_resident()
        #         pass 

        # print(cam.read_camera())

        

        if cv2.waitKey(1) == ord('q'):
            break

    
    cam.end_videostream()
        

            
                
        

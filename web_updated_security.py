import os 
import sys  
import RPi.GPIO as GPIO
import time
import serial
import smtplib
from dotenv import load_dotenv

load_dotenv()

# sys.path.insert(0, '/home/pi/tflite1')

from tflite_1 import cam 
import cv2 

import socket

HOST = "192.168.1.169" # IP address of your Raspberry PI
PORT = 65431          # Port to listen on (non-privileged ports are > 1023)
power = 40
size = 1024


accepted_cards = ['2A CF 7D 19', '2A 60 A9 15']

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)

GPIO.setup(4, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)

armed = False 
armed_pin = 3
disarmed_pin = 4

notification_delay = 15
need_to_notify = True

security_breached = False 

door_opened = False 
person_found = False 

print("Setup Complete")

def allow_exit_delay(delay=1, exit_time=15):
    for i in range(exit_time):
        GPIO.output(armed_pin, False) 
        time.sleep(delay)
        GPIO.output(armed_pin, True)
        time.sleep(delay) 
    if not armed:
        GPIO.output(armed_pin, False) 



def toggle_system():
    global armed 
    armed = not armed

    
    GPIO.output(armed_pin, armed)
    GPIO.output(disarmed_pin, not armed)

    if armed:
        allow_exit_delay(exit_time=2)

def arm_system():
    global armed 
    armed = True 

    GPIO.output(armed_pin, True)
    GPIO.output(disarmed_pin, False)

    allow_exit_delay(exit_time=2)

def disarm_system():
    global armed 
    armed = False 

    GPIO.output(armed_pin, False)
    GPIO.output(disarmed_pin, True)


def alert_resident(message= "Security Breached in your Home"):
    final_message = 'Subject: {}\n\n{}'.format("Home Security Alert: {}".format(time.time()), message)

    USER = os.getenv('USERNAME')
    PASSWORD = os.environ.get('PASSWORD')

    server = smtplib.SMTP_SSL("smtp.gmail.com", 465)
    server.login(USER, PASSWORD)
    server.sendmail(USER, "praval.telagi@gmail.com", final_message)
    server.quit()

def flash_alert(num_flashes=5, delay=0.2):

    for i in range(num_flashes):
        GPIO.output(armed_pin, False) 
        time.sleep(delay)
        GPIO.output(armed_pin, True)
        time.sleep(delay) 
    
    if not armed:
        GPIO.output(armed_pin, False) 


if __name__ == '__main__':

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        # s.settimeout(0.1)
        s.bind((HOST, PORT))
        s.listen()

        try:

            ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)
            ser.flush()

            cur_delay_counter = 0 

            GPIO.output(armed_pin, False) 
            GPIO.output(disarmed_pin, True) 

            while True:
                try:
                    client, clientInfo = s.accept()
                    # print("server recv from: ", clientInfo)
                    data = client.recv(size).decode('UTF-8').strip()      # receive 1024 Bytes of message in binary format
                except Exception as e:
                    print(e)
                    data = "nothing"
                    print('timeout works')

                if data:
                    if data != 'status':
                        print("DATA:", data)
                    if data == 'arm' and not armed:
                        print("System will be armed eventually")
                        arm_system()
                    elif data == 'disarm' and armed:
                        print("System will be disarmed eventually")
                        disarm_system()
                    elif data == 'status':
                        # state_msg = "Armed, Disarmed, Camera"
                        if armed:
                            state_msg = "Armed, "
                            if security_breached:
                                if door_opened:
                                    state_msg += "Door Opened "
                                if person_found:
                                    state_msg += "Movement Found"

                                # state_msg = "Armed, Security Issues Found"
                            else:
                                state_msg = "Armed, None"
                        elif not armed:
                            state_msg = "Disarmed, None"

                        client.sendall(bytes(state_msg, 'utf-8')) # Echo back to client

                if ser.in_waiting > 0:
                    line = ser.readline().decode('utf-8').rstrip()
                    # print(line)
                    
                    if line[10:] in accepted_cards:
                        print("Access Granted")
                        

                        toggle_system() 

                        if armed:
                            print("System Armed:", armed)
                        else:
                            print("System Disarmed")

                            # Reset System State
                            cur_delay_counter = 0
                            need_to_notify = True 
                            security_breached = False 

                        
                    else:
                        print("Access Denied: Informing Residents")

                        alert_resident("Unauthorized Card Tried to Arm/Disarm your System")
                        # if armed:
                        flash_alert()

                input_state = GPIO.input(18)             

                if armed:
                    msg = "Issue Detected:"
                    if input_state == False:
                        print("Door Open")
                        security_breached = True
                        # msg += " Door Opened"

                        door_opened = True 
            
                    objects_found_from_camera = cam.read_camera()
                    if ('person' in objects_found_from_camera):
                        print("Person Detected")
                        security_breached = True 
                        # msg += " Movement Found"

                        person_found = True 

                    if door_opened:
                        msg += " Door Opened"
                    
                    if person_found:
                        msg += " Movement Found"

                    if security_breached:
                        print("Issue Detected")
                        cur_delay_counter += 1
                        time.sleep(1)

                        if cur_delay_counter > notification_delay and need_to_notify:
                            print("Alerting Residents")
                            # print("MESSAGE:", msg)
                            alert_resident(message=msg)
                            need_to_notify = False 

        except KeyboardInterrupt: 
            print("Closing socket")
            client.close()
            s.close()    

    

    cam.end_videostream()
        

            
                
        
